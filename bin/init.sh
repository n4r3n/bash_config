#!/usr/bin/env bash

BASH_CONFIG_HOME="${XDG_CONFIG_HOME:-$HOME/.config}/bash"

for config in 'bash_profile' 'bashrc'; do
    if [[ $(readlink -f "${HOME}/.${config}") = "${BASH_CONFIG_HOME}/${config}" ]]; then
        echo "Nothing to do: symlink for .${config} is already in place"
    else
        if [[ -e "${HOME}/.${config}" ]]; then
            mv "${HOME}/.${config}" "${HOME}/.${config}.bak" && echo "Backup created for .${config}"
        fi
        ln -s "${BASH_CONFIG_HOME}/${config}" "${HOME}/.${config}" && echo "Symlink for created for .${config}"
    fi
done
if [[ ! -e "${BASH_CONFIG_HOME}/environment.sh" ]]; then
    cp "${BASH_CONFIG_HOME}/environment.sh.template" "${BASH_CONFIG_HOME}/environment.sh"
    echo "enviroment file created from template"
fi
