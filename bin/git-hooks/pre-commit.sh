#!/usr/bin/env sh

repo_path="$(git rev-parse --show-toplevel)"
cd "${repo_path}" || { printf 'ERROR: repo path '\''%s'\'' not found' "${repo_path}"; exit 1; }

rval=0

run() {
    for command; do
        if ! ${command}; then
            rval=1
        fi
    done
}

run "bin/check.sh"

exit "${rval}"
