############################
# ~/.config/bash/functions #
############################
# vim: syntax=sh
# shellcheck shell=bash

fk() {
    local lcmd
    lcmd="$(history -p !!)"
    printf '\n\tsudo %s\n\n' "${lcmd}"
    # shellcheck disable=SC2086
    sudo ${lcmd}
}

alt_cd() {
    if [[ "$#" = 0 ]]; then
        # shellcheck disable=SC2164
        builtin cd "${HOME}"
    elif [[ -d "${1}" ]]; then
        shift "$(($# - 1))"
        # shellcheck disable=SC2164
        builtin cd "${1}"
    elif [[ -e "${1}" ]]; then
        shift "$(($# - 1))"
        printf 'cd: Not a directory, changing to parent directory\n'
        # shellcheck disable=SC2164
        builtin cd "$(dirname "${1}")"
    else
        printf 'cd: No such file or directory\n'
    fi
}

mvr() {
    local file
    for file; do
        if [[ ! -e "${file}" ]]; then
            printf 'mvr: No such file or directory: %s\n' "${file}"
        else
            read -eri "${file}" newfilename
            mv -v -- "${file}" "${newfilename}"
        fi
    done
}

cpn() {
    local file
    for file; do
        if [[ ! -e "${file}" ]]; then
            printf 'cpn: No such file or directory: %s\n' "${file}"
        else
            read -eri "${file}" newfilename
            cp -ivr -- "${file}" "${newfilename}"
        fi
    done
}

_df() {
    if command df -x tmpfs > /dev/null 2>&1; then
        command df -Ph -x tmpfs -x devtmpfs "$@"
    else
        command df -Ph "$@"
    fi
}

dfc() {
    _df "${@}" | sed -E 's/([ 1-6][0-9]%)/\x1b[32m\1\x1b[0m/g; s/([7-8][0-9]%)/\x1b[33m\1\x1b[0m/g; s/((9[0-9]|100)%)/\x1b[31m\1\x1b[0m/g'
}

dfic() {
    _df -i "${@}" | sed -E 's/([ 1-6][0-9]%)/\x1b[32m\1\x1b[0m/g; s/([7-8][0-9]%)/\x1b[33m\1\x1b[0m/g; s/((9[0-9]|100)%)/\x1b[31m\1\x1b[0m/g'
}

sticky() {
    "${EDITOR}" ~/Notebooks/StickyNote.txt
}

ldiff() { # Colored diff in less
    diff --color=always "$@" | less -r
}

ldiffy() { # Colored side-by-side diff in less
    diff -y --color=always "$@" | less -r
}

mkcd() {
    mkdir -pv "$@" || return
    shift "$(($# - 1))"
    cd -- "${1}" || exit 1
}

dus() { # Show disk usage sorted by file size
    du -hsPx "$@" | sort -h
}

rtfm() {
    # Credit: https://gist.github.com/evverx/991979b28046d9ee5ab2
    [[ -z "${1}" ]] && return
    local type
    type=$(type -t "$1")
    case "${type}" in
        builtin|keyword)
            help "$1";;
        alias)
            alias "$1";;
        function)
            type "$1";;
        file)
            man "$1";; # "$1" --help, "$1" -h, info "$1", google
        *)
            man -k "$1"; info -k "$1";; # google
    esac
}

kindle-usbnet() {
    local interface="${1}"
    sudo -E sh -c "{
        ip link set dev ${interface} down;
        ip link set dev ${interface} address 48:b3:35:3e:96:9b;
        ip addr add 192.168.15.201 dev  ${interface};
        ip link set dev ${interface} up;
    }"
    ssh root@192.168.15.244
}

alert() {
    local icon
    # shellcheck disable=SC2181
    if [[ $? = 0 ]]; then
        icon=terminal
    else
        icon=error
    fi
    last_cmd="$(history | tail -n1 | sed 's/^\s*[0-9]*\s*//' | sed 's/;\s*alert\s*$//')"
    notify-send -i $icon "Command finished:" "$last_cmd"
}

fixmod() {
    find . -type f -exec chmod u+rw,g-w,o-rwx {} + # Set file permissions to 664
    find . -type d -exec chmod u+rwx,g-w,o-rwx {} + # Set folder permissions to 775
}

#rnm() { # Alternative rename command
#    file="${1}"
#    newname="${2}"
#    ext="${file##*.}"
#    mv "${file}" "${newname}.${ext}"
#}

if type -P tmux > /dev/null 2>&1; then
    init-tmux() {
        cd "${HOME}"
        if [[ "$(pgrep -c 'tmux: server')" -gt 0 ]]; then
            exec tmux attach -t "$(tmux list-sessions | cut -d':' -f1 | head -1)"
        else
            exec tmux
        fi
    }
fi

if type -P lowdown > /dev/null 2>&1; then
    mdr() {
        lowdown -Tterm "${1}" --term-columns="$(tput cols)" | less -r
    }
fi

if type -P gpg > /dev/null 2>&1; then
    gpg_enc() { # Encrypt files with gpg
        local recipient="${1}"
        shift
        for file in "${@}"; do
            gpg --output "${file}.gpg" --encrypt --recipient "${recipient}" "${file}"
        done
    }

    gpg_dec() { # Decrypt files with gpg
        for file in "${@}"; do
            gpg --output "${file#*.}" --decrypt "${file}"
        done
    }
fi

if type -P pacman > /dev/null 2>&1; then
    paclean() {
        sudo su -c "{
            #tar -cjf /store/bak/pacman-database.tar.bz2 /var/lib/pacman/local
            pacman -Rscn $(pacman -Qtdq);
            pacman -Sc;
            pacman-optimize;
        }"
        sync
    }
fi

if type -P pdftk > /dev/null 2>&1; then
    pdfnopw() { # Remove password from pdf file
        local file password newfile
        file="${1}"
        read -rp 'Enter documents password: ' password
        newfile="${file%.*}_nopw.${file##*.}"
        pdftk "${file}" output "${newfile}" user_pw "${password}"
    }
fi

if type -P primusrun > /dev/null 2>&1; then
    primus_picom() {
        killall picom
        while pgrep -x picom >/dev/null; do
            sleep 1
        done
        primusrun "$@"
    }
fi

