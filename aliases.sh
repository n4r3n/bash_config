##########################
# ~/.config/bash/aliases #
##########################
# vim: syntax=sh
# shellcheck shell=bash

## System #############
########################
if type -P sudo > /dev/null 2>&1; then
    alias sudo='sudo -E '
    alias f!='sudo bash -c "$(history -p !!)"'
fi
if type -P doas > /dev/null 2>&1; then
    alias doas='doas '
    alias F!='doas bash -c "$(history -p !!)"'
fi

if type -P progress > /dev/null 2>&1; then
    alias progress='progress -M'
fi

if type -P systemctl > /dev/null 2>&1; then
    alias userctl='systemctl --user'
fi

## Connection ##########
########################
if type -p mmcli > /dev/null 2>&1; then
    alias mmForce4G='sudo mmcli -m 0 --set-allowed-modes=4g'
fi

## Package management ##
########################
if type -P pacman > /dev/null 2>&1; then
#    alias chrsh='arch-nspawn $MAKEPKG_CHROOT/root --bind-ro=$LOCAL_REPO bash'
    alias chrsh='arch-nspawn /data/makepkg-chroot/chroot/root --bind-ro=/data/repo bash'
#   alias chrover='arch-nspawn $MAKEPKG_CHROOT/root --bind-ro=$LOCAL_REPO --'
fi

## File operations #####
########################
if type -P rsync > /dev/null 2>&1; then
    alias cprs='rsync -auvEx --xattrs'
    alias sysrsync='rsync -aAXv --xattrs'
fi

# Preserve xattr - GNU coreutils
if cp --help 2>&1 | grep -q 'GNU coreutils'; then
    if cp --preserve=xattr 2>&1 | grep -q -e 'built without xattr support'; then
        alias cp='cp -ivrx'
    else
        alias cp='cp -ivrx --preserve=xattr'
    fi
else
        alias cp='cp -ivr'
fi

alias cps='cp -s'
alias mv='mv -iv'
alias ln='ln -s'
if rm --help 2>&1 | grep -q 'GNU coreutils'; then
    alias  rm='rm -Ivr --preserve-root --one-file-system'
    alias rmf='rm -Ivrf --preserve-root --one-file-system'
else
    alias  rm='rm -ivr'
    alias rmf='rm -ivrf'
fi
alias mkdir='mkdir -pv'

if tar --help 2>&1 | grep -q -e "GNU 'tar'" -e 'bsdtar'; then
    alias tar='tar --xattrs'
fi

if df --help 2>&1 | grep -q 'GNU coreutils'; then
    alias  df='df -Ph -x tmpfs -x devtmpfs -x efivarfs'
    alias dfi='df -Phi -x tmpfs -x devtmpfs -x efivarfs'
else
    alias  df='df -Ph'
    alias dfi='df -Phi'
fi

if du --help 2>&1 | grep -q 'GNU coreutils'; then
    alias du='du -hsPx'
else
    alias du='du -hsx'
fi

alias lsblk='lsblk -o MODEL,NAME,TYPE,FSTYPE,SIZE,MOUNTPOINT,LABEL,PARTLABEL'

if type -P diff > /dev/null 2>&1; then
    if diff --help 2>&1 | grep -q 'GNU diffutils'; then
        alias diff='diff -u'
        alias diffc='diff --color=always'
        alias diffy='command diff -y --color=always'
    fi
fi

if [[ ! "${SSH_TTY}" == $(tty) ]]; then
    alias scp='scp -r'
fi

if type -P ncdu > /dev/null 2>&1; then
    alias ncdu='ncdu --color dark'
fi

## Listing files #######
########################
if type -t alt_cd > /dev/null 2>&1; then
    alias cd='alt_cd'
fi

alias cdback='cd "${OLDPWD}"'

#If eza is installed use it instead of ls - active fork of exa
if type -P eza > /dev/null 2>&1; then
    alias   ls='eza --color=auto --group-directories-first --icons=auto'
    alias  lsa='eza --color=auto --group-directories-first --icons=auto -a'
    alias  lsl='eza --color=auto --group-directories-first --icons=auto -lg --smart-group'
    alias  lsg='eza --color=auto --group-directories-first --icons=auto -aalg --smart-group --git'
    alias lsgr='eza --color=auto --group-directories-first --icons=auto -aalg --smart-group --git-repos'
    alias lsal='eza --color=auto --group-directories-first --icons=auto -aalg --smart-group'
    alias tree='eza --color=auto --group-directories-first --icons=auto -T'
    alias dree='eza --color=auto --group-directories-first --icons=auto -TD'
else
    alias   ls='ls --color=auto --group-directories-first -p'
    alias  lsa='ls --color=auto --group-directories-first -pa'
    alias  lsl='ls --color=auto --group-directories-first -plh'
    alias lsal='ls --color=auto --group-directories-first -palh'
fi

if type -P tree > /dev/null 2>&1; then
    if ! tree --help 2>&1 | grep -q 'BusyBox'; then
        alias tree='tree --dirsfirst -C'
        alias dree='tree --dirsfirst -Cd'
    fi
fi

## Search ##############
########################
alias grep='grep --color=auto'
alias igrep='grep -v'

## Games ###############
########################
if type -P telnet > /dev/null 2>&1; then
    alias cybersphere='telnet www.cybersphere.net 7777'
    alias sindome='telnet moo.sindome.org 5555'
fi

## curl service ########
########################
if type -P curl > /dev/null 2>&1; then
    alias cheat='curl https://cheat.sh/'
    alias wttr='curl https://wttr.in'
fi

## Clipboard ###########
########################
if [[ "${XDG_SESSION_TYPE}" = 'wayland' ]]; then
    if type -P wl-copy wl-paste > /dev/null 2>&1; then
        alias ccp='wl-copy'
        alias cpt='wl-paste'
    fi
elif [[ -n "${DISPLAY}" ]]; then
    if type -P xsel > /dev/null 2>&1; then
        alias ccp='xsel --clipboard --input'
        alias cpt='xsel --clipboard --output'
    fi
fi

#alias imgclip='xclip -selectio -t image/png <'
#alias xclip2='xclip -selectio c'
#alias xpaste='xclip -o >'

## Editors #############
########################
# Better solution is to use symlinks and scripts, see:
#    https://aur.archlinux.org/packages/neovim-symlinks/ or
#    https://aur.archlinux.org/packages/neovim-drop-in/
#    set alias value in 'environment' file
if "${NEOVIM_ALIAS:-false}" && type -P nvim > /dev/null 2>&1 ; then
    alias edit='nvim'
    alias view='nvim -R'
    alias vi='nvim'
    alias vim='nvim'
    alias vimdiff='nvim -d'
elif type -P vim > /dev/null 2>&1 ; then
    alias vi='vim'
fi

if type -P less > /dev/null 2>&1; then
    if ! less --help 2>&1 | grep -q 'BusyBox'; then
        alias less='less -r'
    fi
fi

## Media players #######
########################
if type -P mpv > /dev/null 2>&1; then
    alias mpa='mpv --no-video'
fi

if type -P scrot > /dev/null 2>&1 && [[ -n "${DISPLAY}" ]]; then
    alias scrot='scrot -cz -d 3'
fi

## Apps ################
########################
#alias conky='conky -d'
if type -P wget > /dev/null 2>&1; then
    if wget --help 2>&1 | grep -q 'GNU Wget'; then
        alias wget='wget --xattr'
    fi
fi

if type -P ip > /dev/null 2>&1; then
    alias ip='ip -c'
    alias ipx='ip -c -br'
fi

if type -P nmap > /dev/null 2>&1; then
    alias netdisc='nmap -sP'
fi

if type -P udiskctl > /dev/null 2>&1; then
    alias mntiso='udisksctl loop-setup -r -f'
fi

if type -P radeontop > /dev/null 2>&1; then
    alias radeontop='radeontop -cT'
fi

alias rexif='exiftool -all='
alias feh='feh --scale-down'
alias xkey='xdotool key'
alias x-off='xset dpms force off'
