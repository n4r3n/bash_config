########################
# ~/.bashrc ############
########################
# vim: syntax=sh
# shellcheck shell=bash

export BASH_CONFIG_HOME="${XDG_CONFIG_HOME:-$HOME/.config}/bash"

## If not running interactively, don't do anything
##################################################
if [[ $- != *i* ]]; then
    return
fi

# Automatically start termux if bash is running in TERMINAL, if termux isn't running, or it has no clients connected
if type -P tmux > /dev/null 2>&1; then
    if [[ "$(tty)" =~ '/dev/pts/' ]] && ps -p "${PPID}" | grep -q "${TERMINAL}"; then
        if [[ "$(pgrep -U "$(id -u)" 'tmux: server' -c)" -eq 0 ]]; then
            exec tmux
        elif [[ "$(pgrep -U "$(id -u)" 'tmux: client' -c)" -eq 0 ]]; then
            exec tmux attach -t "$(tmux list-sessions | cut -d':' -f1 | head -1)"
        fi
    fi
fi

## Load external sources
########################

# shellcheck source=./environment.sh
# shellcheck source=./functions.sh
# shellcheck source=./aliases.sh
# shellcheck source=./tools.sh
# shellcheck source=./prompt.sh
# shellcheck source=./prompt_ps4.sh
config_files=('environment.sh'
            'functions.sh'
            'aliases.sh'
            'tools.sh'
            'prompt.sh'
            'prompt_ps4.sh')

for file in "${config_files[@]}"; do
    if [[ -f "${BASH_CONFIG_HOME}/${file}" ]]; then
        # shellcheck disable=SC1090
        # shellcheck disable=SC1091
        source "${BASH_CONFIG_HOME}/${file}"
    fi
done

unset config_files

if "${PS1GIT:-false}" && type -P git > /dev/null 2>&1 && [[ -f "${BASH_CONFIG_HOME}/prompt_git.sh" ]]; then
    # shellcheck source=./prompt_git.sh
    source "${BASH_CONFIG_HOME}/prompt_git.sh"
fi

# History management
###################
HISTFILESIZE='1000'
HISTCONTROL='erasedups'
HISTTIMEFORMAT='%m.%d %H:%M '
HISTIGNORE='f!:F!'
BASH_HISTDIR="${XDG_DATA_HOME:-"${HOME}/.local/share"}/bash"

if [[ ! -d "${BASH_HISTDIR}" ]]; then
    command mkdir -p "${BASH_HISTDIR}"
fi

if [[ "${TMUX}" ]]; then
    if [[ ! -d "${BASH_HISTDIR}/tmux" ]]; then
        command mkdir -p "${BASH_HISTDIR}/tmux"
    fi

    bash_update_history() {
        history -w
        history -c
        history -r
    }

    TMUX_PANE_INDEX="$(tmux display -pt "${TMUX_PANE:?}" '#{session_name}-#{window_index}-#{pane_index}')"

    HISTFILE="${BASH_HISTDIR}/tmux/bash_history-${TMUX_PANE_INDEX}"

else
    HISTFILE="${BASH_HISTDIR}/bash_history"
    bash_update_history() {
        history -n
        history -w
        history -c
        history -r
    }
fi

if ! echo "${PROMPT_COMMAND}" | grep -q 'bash_update_history' ; then
    PROMPT_COMMAND="bash_update_history${PROMPT_COMMAND:+; $PROMPT_COMMAND}"
#    export HISTFILE PROMPT_COMMAND
fi

## Shell options
################
shopt -s autocd             #turn on automatic directory change
shopt -s checkwinsize       #resizeoutput based on terminal window size
#shopt -s histappend
shopt -s histreedit

