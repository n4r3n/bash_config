#############################
# ~/.config/bash/prompt_ps4 #
#############################
# vim: syntax=sh
# shellcheck shell=bash

gen_PS4() {
    #echo -e "${E_green}\${BASH_SOURCE}${E_normal} - ${E_red}L\${LINENO}${E_normal} - \${FUNCNAME[0]:+\${FUNCNAME[0]}()}${E_normal}: \n
    # shellcheck disable=SC2154
    echo -e "${E_green}\${BASH_SOURCE}${E_normal} - ${E_red}L\${LINENO}${E_normal}: \n"
}

gen_PS2() {
    # shellcheck disable=SC2154
    echo "${E_green}  > ${E_normal}"
}

PS4="$(gen_PS4)"
PS2="$(gen_PS2)"
export PS4 PS2

