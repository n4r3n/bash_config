#########################
# ~/.config/bash/colors #
#########################
# vim: syntax=sh
# shellcheck shell=bash

local color_red color_green color_yellow color_cyan color_magenta style_bold stylecolor_reset

export style_bold="\[\e[1m\]"      # bold style

export stylecolor_reset="\[\e[0m\]"    # reset style

export color_red="\[\e[31m\]"      # text color: red
export color_green="\[\e[32m\]"    # text color: green
export color_yellow="\[\e[33m\]"   # text color: yellow
export color_cyan="\[\e[36m\]"     # text color: cyan
export color_magenta="\[\e[35m\]"  # text color: magenta

