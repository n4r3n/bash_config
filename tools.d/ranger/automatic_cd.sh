# vim: syntax=sh
# shellcheck shell=bash

function ranger-cd {
    local tempfile
    tempfile="$(mktemp -t tmp.XXXXXX)"
    ranger --choosedir="${tempfile}" "${@:-$(pwd)}"
    if [[ -f "${tempfile}" ]]; then
        if ! grep -Fxq "$(pwd)" "${tempfile}"; then
            # shellcheck disable=SC2164
            command cd -- "$(cat "${tempfile}")"
        fi
        rm -f -- "${tempfile}"
    fi
}

# This binds Ctrl-O to ranger-cd:
bind '"\C-o":"ranger-cd\C-m"'
