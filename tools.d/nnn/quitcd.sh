# vim: syntax=sh
# shellcheck shell=bash

nnn-cd() {

    export NNN_TMPFILE="${XDG_CONFIG_HOME:-"${HOME}/.config"}/nnn/.lastd"

    if [[ "${NNNLVL:-0}" -ge 1 ]]; then
        echo "nnn is already running"
        return
    fi

    nnn "$@"

    if [[ -f "${NNN_TMPFILE}" ]]; then
        # shellcheck disable=SC1090
        source "${NNN_TMPFILE}"
        true > "${NNN_TMPFILE}"
    fi
}

# This binds Ctrl-O to nnn-cd:
bind '"\C-p":"nnn-cd\C-m"'
