##########################
# ~/.config/bash/scripts #
##########################
# vim: syntax=sh
# shellcheck shell=bash

if type -P fzf > /dev/null 2>&1; then          # Load fzf, the fuzzy finder
    if [[ -e "/usr/share/fzf/key-bindings.bash" ]] &&
        [[ -e "/usr/share/fzf/completion.bash" ]]; then
        # shellcheck disable=SC1091
        source /usr/share/fzf/key-bindings.bash
        # shellcheck disable=SC1091
        source /usr/share/fzf/completion.bash
    elif [[ -e "${BASH_CONFIG_HOME}/tools.d/fzf/key-bindings.bash" ]] &&
        [[ -e "${BASH_CONFIG_HOME}/tools.d/fzf/completion.bash" ]]; then
        # shellcheck disable=SC1090
        # shellcheck disable=SC1091
        source "${BASH_CONFIG_HOME}/tools.d/fzf/key-bindings.bash"
        # shellcheck disable=SC1090
        # shellcheck disable=SC1091
        source "${BASH_CONFIG_HOME}/tools.d/fzf/completion.bash"
    fi
fi

if type -P ranger > /dev/null 2>&1; then       # Load ranger tools
    # shellcheck source=./tools.d/ranger/automatic_cd.sh
    source "${BASH_CONFIG_HOME}/tools.d/ranger/automatic_cd.sh"
    # shellcheck source=./tools.d/ranger/subshell_notice.sh
    source "${BASH_CONFIG_HOME}/tools.d/ranger/subshell_notice.sh"
fi

if type -P nnn > /dev/null 2>&1; then          # Load nnn tools
    # shellcheck source=./tools.d/nnn/quitcd.sh
    source "${BASH_CONFIG_HOME}/tools.d/nnn/quitcd.sh"
fi

if type -P fuck > /dev/null 2>&1; then           # Load thefuck
    eval "$(thefuck --alias)"
fi

# colorize man pages

if type -P man > /dev/null 2>&1; then
    export LESS_TERMCAP_mb=$'\e[1;32m'
    export LESS_TERMCAP_md=$'\e[1;32m'
    export LESS_TERMCAP_me=$'\e[0m'
    export LESS_TERMCAP_se=$'\e[0m'
    export LESS_TERMCAP_so=$'\e[43;1;30m'
    export LESS_TERMCAP_ue=$'\e[0m'
    export LESS_TERMCAP_us=$'\e[1;4;31m'
fi
