########################
# ~/.bash_profile ######
########################
# vim: syntax=sh
# shellcheck shell=bash

if [[ -d "${HOME}/.config/environment.d" ]]; then
    shopt -s nullglob
    set -a
    for conf in "${HOME}/.config/environment.d"/*.conf; do
        # shellcheck disable=SC1090
        source "${conf}"
    done
    set +a
    shopt -u nullglob
fi

if type -P startx > /dev/null 2>&1; then
    if [[ ! "${DISPLAY}" ]] && [[ "$(tty)" = '/dev/tty1' ]]; then
        exec startx -- -keeptty > /dev/null 2>&1
    fi
fi

if [[ -f "${HOME}/.bashrc" ]]; then
    # shellcheck source=./bashrc
    source "${HOME}/.bashrc"
fi
