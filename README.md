# My bash configuration used mainly across my Linux devices

This config is modular. It loads functions and aliases only if the required command is available.

## How to use it

```sh
$ git clone https://codeberg.org/n4r3n/bash_config.git "${XDG_CONFIG_HOME:-$HOME/.config}/bash"
$ "${XDG_CONFIG_HOME:-$HOME/.config}/bash/bin/init.sh"
$ exec bash
```

The file `environment` is not tracked because it can contain API keys or tokens

The `prompt_git` can be slow on low-end devices (e.g. on Android phones in Termux), by default it is turned off, you can turn it on by setting the value of `PS1GIT` to `true` (it's commented out in the file `environment`)

## How to contribute

```sh
$ "${XDG_CONFIG_HOME:-$HOME/.config}/bash/bin/init-repo.sh"
```

It will symlink files from `bin/git-hooks` into the '.git/hooks' directory, automating some things that were previously done by CI/CD.
