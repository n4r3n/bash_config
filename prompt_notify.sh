################################
# ~/.config/bash/prompt_notify #
################################
# vim: syntax=sh
# shellcheck shell=bash

# This script sends notification if a long running command exited
#################################################################

prompt_notify_init() {
    local exception_list='nvim vim vi nano tmux screen ncmpcpp ranger nnn mc mpv bat less man git'
    if ! echo "${exception_list}" | grep -qF -- "${BASH_COMMAND%% *}" ; then
        SECONDS_PR="${SECONDS}"
        COMMAND_PR="${BASH_COMMAND}"
    else
        SECONDS_PR=''
    fi
}

prompt_notify() {
    if [[ -n "${SECONDS_PR}" ]] && [[ $((SECONDS - SECONDS_PR)) -ge 20 ]]; then
        notify-send -i "utilities-terminal" -a "bash" "'${COMMAND_PR}' exited"
    fi
    trap '{ trap DEBUG; prompt_notify_init; }' DEBUG
}

if ! echo "${PROMPT_COMMAND}" | grep -q 'prompt_notify' ; then
    PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }'prompt_notify'
    export PROMPT_COMMAND
fi
