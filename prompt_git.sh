#############################
# ~/.config/bash/prompt_git #
#############################
# vim: syntax=sh
# shellcheck shell=bash

ps1_git() {
## Git repo status

    # shellcheck source=./colors.sh
    source "${BASH_CONFIG_HOME}/colors.sh"

    local gitstatus gitbranch icons rem mod new add
    gitstatus="$(git status --porcelain 2> /dev/null)"

    # shellcheck disable=SC2181
    if [[ "$?" -ne 0 ]]; then
        echo ''
        exit 0
    fi

    # shellcheck disable=SC2001
    gitstatus="$(echo "${gitstatus}" | sed 's/^\s*//gi')"
    gitbranch="$(git symbolic-ref --short HEAD 2> /dev/null || echo "unnamed")"

    icons=""

    if [[ -n "${gitstatus}" ]]; then
        rem="$(echo "${gitstatus}" | grep -c '^D')"
        mod="$(echo "${gitstatus}" | grep -c -e '^M' -e '^R')"
        new="$(echo "${gitstatus}" | grep -c '^A')"
        add="$(echo "${gitstatus}" | grep -c '^??')"
        # shellcheck disable=SC2154
        [[ ! "${rem}" -eq 0 ]] && icons+="${color_red}⨯${rem}"
        # shellcheck disable=SC2154
        [[ ! "${mod}" -eq 0 ]] && icons+="${color_yellow}•${mod}"
        # shellcheck disable=SC2154
        [[ ! "${new}" -eq 0 ]] && icons+="${color_cyan}+${new}"
        # shellcheck disable=SC2154
        [[ ! "${add}" -eq 0 ]] && icons+="${color_magenta}+${add}"
    else
        # shellcheck disable=SC2154
        icons="${color_green}✓"
    fi

    # shellcheck disable=SC2154
    echo " (${style_bold}${gitbranch}${stylecolor_reset}|${icons}${stylecolor_reset})"
}

set_ps1() {
    # shellcheck disable=SC2154
    PS1="[${PS1_BASE}$(ps1_git)]\$ "
}

if ! echo "${PROMPT_COMMAND}" | grep -q 'set_ps1' ; then
    PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }'set_ps1'
    export PROMPT_COMMAND
fi
