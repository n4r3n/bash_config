#########################
# ~/.config/bash/prompt #
#########################
# vim: syntax=sh
# shellcheck shell=bash

ps1_base() {
    # shellcheck source=./colors.sh
    source "${BASH_CONFIG_HOME}/colors.sh"

    local usercolor user hostname host
    ## If user is root change usercolor to red
    if [[ "${EUID}" == 0 ]]; then
        usercolor="${color_red}${style_bold}"
    else
        usercolor="${color_green}${style_bold}"
    fi

    ## Get username and hostname
    if [[ -n "${TERMUX_USER}" ]]; then
        username="${TERMUX_USER}"
        hostname="${TERMUX_HOSTNAME}"
    #    local username=$(termux user)
    #    local hostname=$(getprop net.hostname)
    else
        username="\u"
        hostname="\h"
    fi

    ## Username's style
    user="${usercolor}${username}${stylecolor_reset}"

    ## Detect session type, show hostname only when connecting trough ssh
    if [[ -n "${SSH_TTY}" ]]; then
        host="@${style_bold}${hostname}${stylecolor_reset}"
    elif echo "${hostname}" | grep -q _rescue; then
        host="@${style_bold}${color_red}${hostname}${stylecolor_reset}"
    else
        host=''
    fi

    path="\W"

    echo "${user}${host} ${path}"
}

set_title() {
    local userhost
    if [[ -n "${SSH_TTY}" ]]; then
        userhost="${USER}@${HOSTNAME%%.*}"
    else
        userhost="${USER}"
    fi
    case ${TERM} in
        screen*)
            printf "\033_%s:%s\033\\" "${userhost}" "${PWD/#$HOME/\~}";;
        *)
            printf "\033]0;%s:%s\007" "${userhost}" "${PWD/#$HOME/\~}";;
    esac
}

set_cursor() {
    local cursor
    case "${CURSOR_SHAPE:-block}::${CURSOR_STYLE:-blinking}" in
        block::blinking)
            cursor='\e[1 q';;
        block::solid)
            cursor='\e[2 q';;
        underscore::blinking)
            cursor='\e[3 q';;
        underscore::solid)
            cursor='\e[4 q';;
        bar::blinking)
            cursor='\e[5 q';;
        bar::solid)
            cursor='\e[6 q';;
    esac
    printf '%b' "${cursor}"
}

#ps_base="$(psbase)"

PS1_BASE="$(ps1_base)"

PS1="[${PS1_BASE}]\$ "
export PS1

if ! echo "${PROMPT_COMMAND}" | grep -q -e 'set_title' -e '\033'; then
    PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }'set_title'
    export PROMPT_COMMAND
fi

if ! echo "${PROMPT_COMMAND}" | grep -q -e 'set_cursor'; then
    PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }'set_cursor'
    export PROMPT_COMMAND
fi

